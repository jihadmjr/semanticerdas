import pandas as pd
import math
import rdflib
import string
from rdflib.graph import Graph
from rdflib.namespace import FOAF, RDF, RDFS, XSD, NamespaceManager
from rdflib import BNode, Literal, Namespace
import sys
import string

namespace_manager = NamespaceManager(Graph())
DBO = Namespace("http://dbpedia.org/ontology/")
EX = Namespace("http://example.org/data/")
namespace_manager.bind("dbo", DBO, override=False)
namespace_manager.bind("ex", EX, override=False)

g = Graph()
g.namespace_manager = namespace_manager

imdb_link_prop = EX.imdb_link
content_rating_prop = EX.content_rating
release_year_prop = EX.release_year
imdb_score_prop = EX.imdb_score
film_aspect_ratio_prop = EX.aspect_ratio
color_prop = EX.color
punc_table = dict((ord(char), None) for char in string.punctuation)

def setup():
    # IMDB Link Property
    g.add((imdb_link_prop, RDFS.domain, DBO.Film))
    g.add((imdb_link_prop, RDFS.range, XSD.string))

    # Content Rating
    g.add((content_rating_prop, RDFS.domain, DBO.Film))
    g.add((content_rating_prop, RDFS.range, XSD.string))

    # Release Year
    g.add((release_year_prop, RDFS.subPropertyOf, DBO.year))
    g.add((release_year_prop, RDFS.domain, DBO.Film))

    # IMDB Score
    g.add((imdb_score_prop, RDFS.domain, DBO.Film))
    g.add((imdb_score_prop, RDFS.range, XSD.double))

    # Aspect Ratio
    g.add((film_aspect_ratio_prop, RDFS.domain, DBO.Film))
    g.add((film_aspect_ratio_prop, RDFS.range, XSD.double))

    # Color
    g.add((color_prop, RDFS.domain, DBO.Film))
    g.add((color_prop, RDFS.range, XSD.string))


def main(filename):
    df = pd.read_csv(filename)

    df = df.drop(
        ["num_critic_for_reviews", 
        "director_facebook_likes", 
        "actor_3_facebook_likes", 
        "actor_1_facebook_likes", 
        "num_user_for_reviews", 
        "actor_2_facebook_likes", 
        "movie_facebook_likes",
        "num_voted_users",
        "cast_total_facebook_likes",
        "facenumber_in_poster",
        ], axis=1)
    df = df.fillna("")

    for i in range(df.shape[0]):
        row = df.iloc[i]
        data = ""
        print(i)
        movie_title = str(row["movie_title"]).strip() if row["movie_title"] != "" else None
        genres = str(row["genres"]).strip().split("|") if row["genres"] != "" else None
        plot_keywords = str(row["plot_keywords"]).strip().split("|") if row["plot_keywords"] != "" else None
        
        director_name = str(row["director_name"]).strip() if row["director_name"] != "" else None
        duration = int(row["duration"]) if row["duration"] != "" else None
        budget = int(row["budget"]) if row["budget"] != "" else None
        gross = int(row["gross"]) if row["gross"] != "" else None
        
        actor_1_name = str(row["actor_1_name"]).strip() if row["actor_1_name"] != "" else None
        actor_2_name = str(row["actor_2_name"]).strip() if row["actor_2_name"] != "" else None
        actor_3_name = str(row["actor_3_name"]).strip() if row["actor_3_name"] != "" else None
        
        movie_imdb_link = str(row["movie_imdb_link"]).strip() if row["movie_imdb_link"] != "" else None
        language = str(row["language"]).strip() if row["language"] != "" else None
        country = str(row["country"]).strip() if row["country"] != "" else None
        content_rating = str(row["content_rating"]).strip() if row["content_rating"] != "" else None
        title_year = int(row["title_year"]) if row["title_year"] != "" else None
        imdb_score = float(row["imdb_score"]) if row["imdb_score"] != "" else None
        aspect_ratio = float(row["aspect_ratio"]) if row["aspect_ratio"] != "" else None
        color = str(row["color"]).strip() if row["color"] != "" else None

        movie_node = EX[string.capwords(movie_title).translate(punc_table).replace(" ", "_")]

        g.add((movie_node, RDF.type, DBO.Film))

        # Title
        insert_title_to_graph(movie_node, movie_title)

        # Genres
        if genres: insert_genres_to_graph(movie_node, genres)
        if plot_keywords: insert_genres_to_graph(movie_node, plot_keywords)

        # Director Name
        if director_name: insert_director_name_to_graph(movie_node, director_name)

        # Duration
        if duration: insert_duration_to_graph(movie_node, duration)

        # Budget
        if budget: insert_budget_to_graph(movie_node, budget)

        # Gross
        if gross: insert_gross_to_graph(movie_node, gross)

        # Actors
        if actor_1_name: insert_actor_to_graph(movie_node, actor_1_name)
        if actor_2_name: insert_actor_to_graph(movie_node, actor_2_name)
        if actor_3_name: insert_actor_to_graph(movie_node, actor_3_name)

        # IMDB Link
        if movie_imdb_link: insert_imdb_link_to_graph(movie_node, movie_imdb_link)

        # Language
        if language: insert_language_to_graph(movie_node, language)

        # Country
        if country: insert_country_to_graph(movie_node, country)

        # Content rating
        if content_rating: insert_content_rating_to_graph(movie_node, content_rating)

        # Release year
        if title_year: insert_release_year_to_graph(movie_node, title_year)

        # IMDB Score
        if imdb_score: insert_imdb_score_to_graph(movie_node, imdb_score)

        # Aspect Ratio
        if aspect_ratio: insert_aspect_ratio_to_graph(movie_node, aspect_ratio)

        if color: insert_color_to_graph(movie_node, color)


def insert_title_to_graph(movie_node, title):
    g.add((movie_node, RDFS.label, Literal(title)))

def insert_genres_to_graph(movie_node, genres_list):
    for genre in genres_list:
        genre_node = EX[string.capwords(genre).translate(punc_table).replace(" ", "_") + "_film"]
        g.add((movie_node, DBO.genre, genre_node))
        g.add((genre_node, RDF.type, DBO.MovieGenre))
        g.add((genre_node, RDFS.label, Literal(genre)))

def insert_director_name_to_graph(movie_node, director_name):
    director_node = EX[string.capwords(director_name).translate(punc_table).replace(" ", "_") + "_person"]
    g.add((movie_node, DBO.director, director_node))
    g.add((director_node, RDF.type, DBO.Person))
    g.add((director_node, RDFS.label, Literal(director_name)))

def insert_duration_to_graph(movie_node, duration):
    g.add((movie_node, DBO.duration, Literal(duration, datatype=XSD.double)))

def insert_budget_to_graph(movie_node, budget):
    g.add((movie_node, DBO.budget, Literal(budget, datatype=XSD.double)))

def insert_gross_to_graph(movie_node, gross):
    g.add((movie_node, DBO.gross, Literal(gross, datatype=XSD.double)))

def insert_actor_to_graph(movie_node, actor_name):
    actor_node = EX[string.capwords(actor_name).translate(punc_table).replace(" ", "_") + "_person"]
    g.add((movie_node, DBO.starring, actor_node))
    g.add((actor_node, RDF.type, DBO.Actor))
    g.add((actor_node, RDFS.label, Literal(actor_name)))

def insert_imdb_link_to_graph(movie_node, imdb_link):
    g.add((movie_node, imdb_link_prop, Literal(imdb_link)))

def insert_language_to_graph(movie_node, language):
    language_node = EX[string.capwords(language).translate(punc_table).replace(" ", "_") + "_language"]
    g.add((movie_node, DBO.language, language_node))
    g.add((language_node, RDF.type, DBO.Language))
    g.add((language_node, RDFS.label, Literal(language)))

def insert_country_to_graph(movie_node, country):
    country_node = EX[string.capwords(country).translate(punc_table).replace(" ", "_") + "_place"]
    g.add((movie_node, DBO.country, country_node))
    g.add((country_node, RDF.type, DBO.Country))
    g.add((country_node, RDFS.label, Literal(country)))

def insert_content_rating_to_graph(movie_node, content_rating):
    g.add((movie_node, content_rating_prop, Literal(content_rating)))

def insert_release_year_to_graph(movie_node, year):
    g.add((movie_node, release_year_prop, Literal(year, datatype=XSD.gYear)))

def insert_imdb_score_to_graph(movie_node, imdb_score):
    g.add((movie_node, imdb_score_prop, Literal(imdb_score, datatype=XSD.double)))

def insert_aspect_ratio_to_graph(movie_node, aspect_ratio):
    g.add((movie_node, film_aspect_ratio_prop, Literal(aspect_ratio, datatype=XSD.double)))

def insert_color_to_graph(movie_node, color):
    g.add((movie_node, color_prop, Literal(color)))


if __name__ == "__main__":
    if len(sys.argv) == 3:
        output = sys.argv[2]
    else:
        output = "output2.ttl"
    setup()
    main(sys.argv[1])
    g.serialize(output, format="turtle")