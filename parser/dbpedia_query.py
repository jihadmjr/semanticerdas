from SPARQLWrapper import SPARQLWrapper, JSON


sparql = SPARQLWrapper("http://dbpedia.org/sparql")

q = """
PREFIX dbo: <http://dbpedia.org/ontology/>

SELECT DISTINCT ?name
    WHERE {
    ?person a dbo:Person .
    ?person rdfs:label ?name .
    FILTER(lang(?name) = 'en')
    }
LIMIT 10
"""

# Querying dbr links
# q = """
# PREFIX dbo: <http://dbpedia.org/ontology/>

# SELECT DISTINCT ?name
#     WHERE {
#     ?person a dbo:Person .
#     ?person dbo:birthPlace ?name .
#     }
# LIMIT 10
# """

sparql.setQuery(q)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
res = results['results']['bindings']

print(res)
print("=================================")
# [{'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Andreas Ekberg'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Danilo Tognon'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Lorine Livington Pruette'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Megan Lawrence'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Nikolaos Ventouras'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Peter Ceffons'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Sani ol molk'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Siniša Žugić'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': 'Strength athlete'}}, {'name': {'type': 'literal', 'xml:lang': 'en', 'value': "Trampolino Gigante Corno d'Aola"}}]
# =================================

for item in res:
    print(item['name']['value'])



# for result in results["results"]["bindings"]:
#     print(result["label"]["value"])