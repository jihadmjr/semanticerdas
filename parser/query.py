
from requests.models import PreparedRequest
import requests

req = PreparedRequest()
url = "http://localhost:3030/movies/sparql"
params = {'query': "PREFIX ex: <http://example.org/data/>\
    SELECT ?subject ?class\
    WHERE {\
        ?subject a ?class .\
    }\
    LIMIT 25"}

req.prepare_url(url, params)
req.prepare_headers({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/sparql-results+json'})
req.prepare_method("POST")

s = requests.Session()
resp = s.send(req)
print(resp.text)