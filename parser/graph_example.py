import rdflib
from rdflib.graph import Graph

g = Graph()
g.parse('output.ttl', format="n3")

# res1 = g.query("""SELECT DISTINCT ?x
#                      WHERE {
#                      ?g a ex:Film .
#                      ?g rdfs:label ?x .
#                      FILTER (CONTAINS (LCASE(?x), "the")) .
#                      }
#                      """)
# for x in list(res1)[:10]:
#     print(str(x))

# print("==========================================")

# res2 = g.query("""SELECT DISTINCT ?x
#                      WHERE {
#                      ?g a ex:Person .
#                      ?g rdfs:label ?x .
#                      }
#                      """)
# for x in list(res2)[:10]:
#     print(str(x))

q = """SELECT DISTINCT ?x
                     WHERE {
                     ?g a ex:Person .
                     ?g rdfs:label ?x .
                     }"""

res = g.query(q)
for num, item in enumerate(res):
    print(item)
    if(num >= 5):
        break