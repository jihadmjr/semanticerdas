# IMDB Movie Dataset Parser
# Made by Semantic Cerdas Group @ Semantic Web CSUI 2019
# ======================================================

import pandas as pd
import math
import rdflib
import string
from rdflib.graph import Graph

def main():
    filename = input("Insert input filename: ")
    output_filename = input("Insert output filename: ")
    df = pd.read_csv(filename)

    df = df.drop(
        ["num_critic_for_reviews", 
        "director_facebook_likes", 
        "actor_3_facebook_likes", 
        "actor_1_facebook_likes", 
        "num_user_for_reviews", 
        "actor_2_facebook_likes", 
        "movie_facebook_likes",
        "num_voted_users",
        "cast_total_facebook_likes",
        "facenumber_in_poster",
        ], axis=1)
    df = df.fillna("")

    punc_table = dict((ord(char), None) for char in string.punctuation)
    out = open(output_filename, 'w', encoding="utf8")

    print(
    '''@prefix ex: <http://example.org/data/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix dbc: <http://dbpedia.org/resource/Category:> .
@prefix dbr: <http://dbpedia.org/resource/> .
@prefix dbp: <http://dbpedia.org/property/> .
@prefix dbo: <http://dbpedia.org/ontology/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

    ''', file=out)

    #========================

    persons = []

    for i in range(df.shape[0]):
        row = df.iloc[i]
        data = ""

        movie_title = str(row["movie_title"]).strip() if row["movie_title"] != "" else None
        genres = str(row["genres"]).strip().split("|") if row["genres"] != "" else None
        plot_keywords = str(row["plot_keywords"]).strip().split("|") if row["plot_keywords"] != "" else None
        
        director_name = str(row["director_name"]).strip() if row["director_name"] != "" else None
        duration = int(row["duration"]) if row["duration"] != "" else None
        budget = int(row["budget"]) if row["budget"] != "" else None
        gross = int(row["gross"]) if row["gross"] != "" else None
        
        actor_1_name = str(row["actor_1_name"]).strip() if row["actor_1_name"] != "" else None
        actor_2_name = str(row["actor_2_name"]).strip() if row["actor_2_name"] != "" else None
        actor_3_name = str(row["actor_3_name"]).strip() if row["actor_3_name"] != "" else None
        
        movie_imdb_link = str(row["movie_imdb_link"]).strip() if row["movie_imdb_link"] != "" else None
        language = str(row["language"]).strip() if row["language"] != "" else None
        country = str(row["country"]).strip() if row["country"] != "" else None
        content_rating = str(row["content_rating"]).strip() if row["content_rating"] != "" else None
        title_year = int(row["title_year"]) if row["title_year"] != "" else None
        imdb_score = float(row["imdb_score"]) if row["imdb_score"] != "" else None
        aspect_ratio = float(row["aspect_ratio"]) if row["aspect_ratio"] != "" else None
        color = str(row["color"]).strip() if row["color"] != "" else None
        
        #-----------------------------------------------------------------------------------------
        data += "ex:{} a ex:Film ;\n".format(movie_title.translate(punc_table).replace(" ","_"))
        data += '\tex:title "{}" ;\n'.format(movie_title)
        if(genres):
            for item in genres:
                data += '\tex:genre "{}" ;\n'.format(item)
        if(plot_keywords):
            for item in plot_keywords:
                data += '\tex:genre "{}" ;\n'.format(item)
        if(director_name):
            data += '\tex:director ex:{} ;\n'.format(director_name.translate(punc_table).replace(" ","_"))
        if(duration):
            data += '\tex:runtime "{}"^^xsd:integer ;\n'.format(duration)
        if(budget):
            data += '\tex:budget "{}"^^xsd:integer ;\n'.format(budget)
        if(gross):
            data += '\tex:gross "{}"^^xsd:integer ;\n'.format(gross)
        if(actor_1_name):
            data += '\tex:starring ex:{} ;\n'.format(actor_1_name.translate(punc_table).replace(" ","_"))
        if(actor_2_name):
            data += '\tex:starring ex:{} ;\n'.format(actor_2_name.translate(punc_table).replace(" ","_"))
        if(actor_3_name):
            data += '\tex:starring ex:{} ;\n'.format(actor_3_name.translate(punc_table).replace(" ","_"))
        if(movie_imdb_link):
            data += '\tex:imdb_link "{}" ;\n'.format(movie_imdb_link)
        if(language):
            data += '\tex:language "{}" ;\n'.format(language)
        if(country):
            data += '\tex:country "{}" ;\n'.format(country)
        if(content_rating):
            data += '\tex:content_rating "{}" ;\n'.format(content_rating)
        if(title_year):
            data += '\tex:release_year "{}"^^xsd:integer ;\n'.format(title_year)
        if(imdb_score):
            data += '\tex:imdb_score "{}"^^xsd:double ;\n'.format(imdb_score)
        if(aspect_ratio):
            data += '\tex:aspect_ratio "{}"^^xsd:double ;\n'.format(aspect_ratio)
        if(color):
            data += '\tex:color "{}" ;\n'.format(color)
        
        data += '\trdfs:label "{}" .\n'.format(movie_title)
        
        #---------------------------------------------------------------------------------------------
        if(director_name and director_name not in persons):
            clean_name = director_name.translate(punc_table).replace(" ","_")
            data += 'ex:{} a ex:Person ;\n'.format(clean_name)
            data += '\trdfs:label "{}" .\n'.format(director_name)
            persons.append(director_name)
            
        if(actor_1_name  and actor_1_name not in persons):
            clean_name = actor_1_name.translate(punc_table).replace(" ","_")
            data += 'ex:{} a ex:Person ;\n'.format(clean_name)
            data += '\trdfs:label "{}" .\n'.format(actor_1_name)
            persons.append(actor_1_name)
            
        if(actor_2_name  and actor_2_name not in persons):
            clean_name = actor_2_name.translate(punc_table).replace(" ","_")
            data += 'ex:{} a ex:Person ;\n'.format(clean_name)
            data += '\trdfs:label "{}" .\n'.format(actor_2_name)
            persons.append(actor_2_name)
            
        if(actor_3_name  and actor_3_name not in persons):
            clean_name = actor_3_name.translate(punc_table).replace(" ","_")
            data += 'ex:{} a ex:Person ;\n'.format(clean_name)
            data += '\trdfs:label "{}" .\n'.format(actor_3_name)
            persons.append(actor_3_name)
        
        print(data, file=out)
        
    print(
"""
ex:title a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Title" .

ex:genre a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Genre" .

ex:director a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range ex:Person ;
    rdfs:label "Director" .

ex:runtime a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:integer ;
    rdfs:label "Runtime" .

ex:budget a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:integer ;
    rdfs:label "Budget" .

ex:gross a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:integer ;
    rdfs:label "Gross" .

ex:starring a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range ex:Person ;
    rdfs:label "Starring" .

ex:imdb_link a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "IMDB Link" .

ex:language a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Language" .

ex:country a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Country" .

ex:content_rating a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Content rating" .

ex:release_year a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:integer ;
    rdfs:label "Release year" .

ex:imdb_score a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:double ;
    rdfs:label "IMDB score" .

ex:aspect_ratio a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:double ;
    rdfs:label "Aspect ratio" .

ex:color a rdf:Property ;
    rdfs:domain ex:Film ;
    rdfs:range xsd:string ;
    rdfs:label "Color scheme" .
    
ex:Person a rdf:Class ;
    rdfs:label "Person" .

ex:Film a rdf:Class ;
	rdfs:label "Film" .""", file=out)

    out.flush()

if __name__ == "__main__":
    main()
