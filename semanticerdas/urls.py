"""semanticerdas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from search import views as search_views


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', search_views.index, name='index'),
    path('search/<keyword>', search_views.search, name='search'),
    # url(r'^search/([\s\S]*)/$', search_views.search, name='search'),
    url(r'^detail/([\s\S]*)/([\s\S]*)/([\s\S]*)/$', search_views.detail, name='detail')
    
]
