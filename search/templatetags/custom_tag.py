from django import template
import base64

register = template.Library()

@register.filter()
def encode(item):
    enc_bytes = base64.urlsafe_b64encode(item.encode("utf-8"))
    enc_str = str(enc_bytes, "utf-8")
    return enc_str

@register.filter()
def decode(item):
    dec_bytes = base64.b64decode(item)
    dec_str = str(dec_bytes, "utf-8")
    return dec_str