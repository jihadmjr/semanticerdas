from django.shortcuts import render, redirect
from django.http import JsonResponse
from .semantic_helper import Helper
import base64
from .templatetags.custom_tag import encode, decode
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}

@csrf_exempt
def index(request):
    if request.method == "POST":
        keyword = request.POST['keyword']
        return redirect('search', keyword)
    return render(request, 'search.html', response)

# TODO: return render sebuah page berisi list opsi-opsi yang memungkinkan,
# di tiap item itu, tambahin link yang nge refer ke fungsi detail dibawah
# urlnya liat urls.py, fungsi detail nerima 3 parameter
# 1. type, bisa 'movie', 'person', atau 'genre' tanpa kutip
# 2. link db yg local
# 3. link db yg remote
# pastiin itu ke pass ke param
def search(request, keyword):
    helper = Helper()
    subjects = {}
    matching_subjects = helper.get_matching_subjects(keyword)

    # encodedBytes = base64.b64encode(data.encode("utf-8"))
    # encodedStr = str(encodedBytes, "utf-8")
    # print(matching_subjects)
    for subject_type, content in matching_subjects.items():
        for name, links in content.items():
            if subject_type in subjects:
                subjects[subject_type].append({
                "type": subject_type,
                "name": name,
                "local_link": encode(links[0]),
                "remote_link": encode(links[1]) if len(links) > 1 else ""
            })
            else:
                subjects[subject_type] = [{
                "type": subject_type,
                "name": name,
                "local_link": encode(links[0]),
                "remote_link": encode(links[1]) if len(links) > 1 else ""
            }]
            # subjects.append({
            #     "type": subject_type,
            #     "name": name,
            #     "local_link": encode(links[0]),
            #     "remote_link": encode(links[1]) if len(links) > 1 else ""
            # })
    
    response = {
        "subjects": subjects,
        "keyword": keyword
    }
    print(response)
    return render(request, 'subject_list.html', response)

def detail(request, subject_type, local_link, remote_link):
    local_link = decode(local_link)
    remote_link = decode(remote_link)

    helper = Helper()
    if(subject_type.lower() == "movie"):
        data = helper.get_movie_detail(local_link, remote_link)
        if(remote_link and remote_link != ""):
            data["original_url"] = remote_link
        if("duration" in data):
            data["duration"] = eval(str(data["duration"]))
        if("budget" in data):
            data["budget"] = eval(str(data["budget"])) / 1000000
        if("gross" in data):
            data["gross"] = eval(str(data["gross"])) / 1000000
        if("aspect_ratio" in data):
            data["aspect_ratio"] = eval(str(data["aspect_ratio"]))
        if("imdb_score" in data):
            data["imdb_score"] = eval(str(data["imdb_score"]))

        return render(request, 'detail_movie.html', data)
        return JsonResponse(data)
    elif(subject_type.lower() == "person"):
        data = helper.get_person_detail(local_link, remote_link)
        if(remote_link and remote_link != ""):
            data["original_url"] = remote_link
        return render(request, 'detail_person.html', data)
