import rdflib
from rdflib.graph import Graph
from SPARQLWrapper import SPARQLWrapper, JSON

class Helper():
    def __init__(self):
        self.local_sparql = SPARQLWrapper("http://localhost:3030/staging/sparql")
        self.remote_sparql = SPARQLWrapper("http://dbpedia.org/sparql")

    def get_matching_movies(self, keyword):
        keyword = keyword.lower()
        result = {}

        q_local = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?title ?director_name ?link
WHERE {
    ?link a dbo:Film .
    ?link rdfs:label ?title .
    ?link dbo:director ?director .
    ?director rdfs:label ?director_name .""" + """
    FILTER (CONTAINS (LCASE(?title), "{}")) .""".format(keyword) + """
}"""

        q_remote = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?title ?director_name ?link
WHERE {
    ?link a dbo:Film .
    ?link foaf:name ?title .
    FILTER(lang(?title) = 'en') .
    ?link dbo:director ?director .
    ?director rdfs:label ?director_name .""" + """
    FILTER (CONTAINS (LCASE(xsd:string(?title)), "{}")) .""".format(keyword) + """
    FILTER (lang(?director_name) = 'en') .
}
"""
        self.local_sparql.setQuery(q_local)
        self.local_sparql.setReturnFormat(JSON)
        local_res = self.local_sparql.query().convert()['results']['bindings']

        for item in local_res:
            key = item["title"]["value"] + " - " + item["director_name"]["value"]
            val = item["link"]["value"]
            result[key] = [val,""]
        
        self.remote_sparql.setQuery(q_remote)
        self.remote_sparql.setReturnFormat(JSON)
        remote_res = self.remote_sparql.query().convert()['results']['bindings']

        for item in remote_res:
            key = item["title"]["value"] + " - " + item["director_name"]["value"]
            val = item["link"]["value"]
            if key in result:
                result[key][1] = val
            else:
                result[key] = ["",val]

        return result

    def get_matching_person(self, keyword):
        keyword = keyword.lower()
        result = {}

        q_local = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?name ?link
WHERE {
    { ?link a dbo:Person }
    UNION
    { ?link a dbo:Actor } .
    ?link rdfs:label ?name .""" + """
    FILTER (CONTAINS (LCASE(?name), "{}")) .""".format(keyword) + """
}"""

        q_remote = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?name ?link
WHERE {
    ?link a dbo:Person .
    ?link rdfs:label ?name .
    ?film a dbo:Film .
    { ?film dbo:starring ?link }
UNION
    { ?film dbo:director ?link }""" + """
    FILTER (CONTAINS (LCASE(?name), "{}")) .""".format(keyword) + """
    FILTER (lang(?name) = 'en') .
}
"""
        self.local_sparql.setQuery(q_local)
        self.local_sparql.setReturnFormat(JSON)
        local_res = self.local_sparql.query().convert()['results']['bindings']

        for item in local_res:
            key = item["name"]["value"]
            val = item["link"]["value"]
            result[key] = [val, ""]
        
        self.remote_sparql.setQuery(q_remote)
        self.remote_sparql.setReturnFormat(JSON)
        remote_res = self.remote_sparql.query().convert()['results']['bindings']

        for item in remote_res:
            key = item["name"]["value"]
            val = item["link"]["value"]
            if key in result:
                result[key][1] = val
            else:
                result[key] = ["",val]

        return result

    def get_matching_subjects(self, keyword):
        keyword = keyword.lower()
        result = {}
        result["movie"] = self.get_matching_movies(keyword)
        result["person"] = self.get_matching_person(keyword)
        return result

    def get_movie_detail(self, local_link, remote_link):
        result = {}

        if(local_link != None and local_link != ""):
            q_local_info = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?title ?director_name ?director_link ?budget ?country ?duration ?gross ?language ?aspect_ratio ?color ?content_rating ?imdb_link ?imdb_score ?release_year
WHERE { """ +"""
    <{}> rdfs:label ?title .""".format(local_link) + """
    <{}> dbo:director ?director_link .""".format(local_link) + """
    ?director_link rdfs:label ?director_name .""" + """
    <{}> dbo:budget ?budget .""".format(local_link) + """
    <{}> dbo:country ?c .""".format(local_link) + """
    ?c rdfs:label ?country .""" + """
    <{}> dbo:duration ?duration .""".format(local_link) + """
    <{}> dbo:gross ?gross .""".format(local_link) + """
    <{}> dbo:language ?l .""".format(local_link) + """
    ?l rdfs:label ?language .""" + """
    <{}> ex:aspect_ratio ?aspect_ratio .""".format(local_link) + """
    <{}> ex:color ?color .""".format(local_link) + """
    <{}> ex:content_rating ?content_rating .""".format(local_link) + """
    <{}> ex:imdb_link ?imdb_link .""".format(local_link) + """
    <{}> ex:imdb_score ?imdb_score .""".format(local_link) + """
    <{}> ex:release_year ?release_year .""".format(local_link) + """
}"""

            self.local_sparql.setQuery(q_local_info)
            self.local_sparql.setReturnFormat(JSON)
            local_res = self.local_sparql.query().convert()['results']['bindings'][0]

            result["title"] = local_res["title"]["value"]
            result["director_name"] = local_res["director_name"]["value"]
            result["director_link"] = [local_res["director_link"]["value"],""]
            result["budget"] = local_res["budget"]["value"]
            result["country"] = local_res["country"]["value"]
            result["duration"] = local_res["duration"]["value"]
            result["gross"] = local_res["gross"]["value"]
            result["language"] = local_res["language"]["value"]
            result["aspect_ratio"] = local_res["aspect_ratio"]["value"]
            result["color"] = local_res["color"]["value"]
            result["content_rating"] = local_res["content_rating"]["value"]
            result["imdb_link"] = local_res["imdb_link"]["value"]
            result["imdb_score"] = local_res["imdb_score"]["value"]
            result["release_year"] = local_res["release_year"]["value"]
        
        if(remote_link != None and remote_link != ""):
            q_remote_info = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?title ?director_name ?director_link ?budget ?country ?gross ?language ?abstract
WHERE { """ +"""
    <{}> foaf:name ?title .""".format(remote_link) + """
    FILTER(lang(?title) = 'en')
    <{}> dbo:director ?director_link .""".format(remote_link) + """
    ?director_link rdfs:label ?director_name .""" + """
    FILTER(lang(?director_name) = 'en') .""" + """
    <{}> dbo:abstract ?abstract .""".format(remote_link) + """
    FILTER(lang(?abstract) = 'en') .""" + """
    OPTIONAL { """+ "<{}>".format(remote_link) + """ dbo:budget ?budget } . """ + """
    OPTIONAL { """+ "<{}>".format(remote_link) + """ dbo:country ?country } . """ + """
    OPTIONAL { """+ "<{}>".format(remote_link) + """ dbo:gross ?gross } . """ + """
    OPTIONAL { """+ "<{}>".format(remote_link) + """ dbp:country ?country } . """ + """
    OPTIONAL { """+ "<{}>".format(remote_link) + """ dbp:language ?language } . """ + """
}"""

            self.remote_sparql.setQuery(q_remote_info)
            self.remote_sparql.setReturnFormat(JSON)
            remote_res = self.remote_sparql.query().convert()['results']['bindings'][0]

            if "title" not in result.keys():
                result["title"] = remote_res["title"]["value"]
            if "director_name" not in result.keys():
                result["director_name"] = remote_res["director_name"]["value"]
            if "director_link" not in result.keys():
                result["director_link"] = ["",remote_res["director_link"]["value"]]
            else: 
                result["director_link"][1] = remote_res["director_link"]["value"]
            if "country" not in result.keys():
                result["country"] = remote_res["country"]["value"] 
            if "budget" not in result.keys() and "budget" in remote_res:
                result["budget"] = remote_res["budget"]["value"] 
            if "gross" not in result.keys() and "gross" in remote_res:
                result["gross"] = remote_res["gross"]["value"]  
            if "country" not in result.keys() and "country" in remote_res:
                result["country"] = remote_res["country"]["value"]  
            if "language" not in result.keys() and "language" in remote_res:
                result["language"] = remote_res["language"]["value"]  
            result["abstract"] = remote_res["abstract"]["value"]

        if(local_link != None and local_link != ""):
            q_local_starring = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?starring ?starring_link
WHERE { """ + """
    <{}> dbo:starring ?starring_link .""".format(local_link) + """
    ?starring_link rdfs:label ?starring .""" + """
}"""

            self.local_sparql.setQuery(q_local_starring)
            self.local_sparql.setReturnFormat(JSON)
            local_res = self.local_sparql.query().convert()['results']['bindings']
            starring = {}

            for item in local_res:
                starring[item["starring"]["value"]] = [item["starring_link"]["value"], ""]
            result["starring"] = starring

        if(remote_link != None and remote_link != ""):
            q_remote_starring = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?starring ?starring_link
WHERE { """ + """
    <{}> dbo:starring ?starring_link .""".format(remote_link) + """
    ?starring_link rdfs:label ?starring .
    FILTER(lang(?starring)='en')""" + """
}"""

            self.remote_sparql.setQuery(q_remote_starring)
            self.remote_sparql.setReturnFormat(JSON)
            remote_res = self.remote_sparql.query().convert()['results']['bindings']
            if("starring" in result):
                starring = result["starring"]
            else:
                starring = {}

            for item in remote_res:
                if(item["starring"]["value"] in starring):
                    starring[item["starring"]["value"]][1] = item["starring_link"]["value"]
                else:
                    starring[item["starring"]["value"]] = ["",item["starring_link"]["value"]]
            result["starring"] = starring

        if(local_link != None and local_link != ""):
            q_local_genre = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?genre ?genre_link
WHERE { """ + """
    <{}> dbo:genre ?genre_link .""".format(local_link) + """
    ?genre_link rdfs:label ?genre .""" + """
}"""


            self.local_sparql.setQuery(q_local_genre)
            self.local_sparql.setReturnFormat(JSON)
            local_res = self.local_sparql.query().convert()['results']['bindings']
            genres = {}

            for item in local_res:
                genres[item["genre"]["value"]] = item["genre_link"]["value"]
            result["genres"] = genres

        return result

    def get_person_detail(self, local_link, remote_link):
        result = {}
        
        if(local_link != None and local_link != ""):
            q_local_info = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT DISTINCT ?name 
WHERE { """ + """
    <{}> rdfs:label ?name .""".format(local_link) + """
}"""
        
            self.local_sparql.setQuery(q_local_info)
            self.local_sparql.setReturnFormat(JSON)
            local_res = self.local_sparql.query().convert()['results']['bindings'][0]

            result["name"] = local_res["name"]["value"]

        if(remote_link != None and remote_link != ""):
            q_remote_info = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT DISTINCT ?name ?abstract
WHERE { """ + """
    <{}> rdfs:label ?name .""".format(remote_link) + """
    FILTER(lang(?name) = 'en') .""" + """
    <{}> dbo:abstract ?abstract .
    FILTER(lang(?abstract) = 'en') .""".format(remote_link) + """
}"""
        

            self.remote_sparql.setQuery(q_remote_info)
            self.remote_sparql.setReturnFormat(JSON)
            remote_res = self.remote_sparql.query().convert()['results']['bindings'][0]

            result["name"] = remote_res["name"]["value"]
            result["abstract"] = remote_res["abstract"]["value"]

        if(local_link != None and local_link != ""):
            q_local_movie = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?movie_name ?director_name ?movie_link
WHERE { """ + """
    ?movie_link a dbo:Film .
    ?movie_link rdfs:label ?movie_name .
    ?movie_link dbo:director ?d .
    ?d rdfs:label ?director_name .""" + """
    ?movie_link dbo:starring <{}> .""".format(local_link) + """
}"""

            self.local_sparql.setQuery(q_local_movie)
            self.local_sparql.setReturnFormat(JSON)
            local_res = self.local_sparql.query().convert()['results']['bindings']

            movies = {}
            for item in local_res:
                key = item["movie_name"]["value"] + " - " + item["director_name"]["value"]
                val = item["movie_link"]["value"]
                movies[key] = [val,""]
            
            result["movies"] = movies

        if(remote_link != None and remote_link != ""):
            q_remote_movie = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?title ?director_name ?link
WHERE {
    ?link a dbo:Film .
    ?link foaf:name ?title .
    FILTER(lang(?title) = 'en') .
    ?link dbo:director ?director .
    ?director rdfs:label ?director_name .
    {?link dbo:starring <""" + "{}".format(remote_link) + """>} """ + """
    UNION 
    {?link dbo:director <""" + "{}".format(remote_link) + """>} .""" + """
    FILTER (lang(?director_name) = 'en') .
}
"""
            self.remote_sparql.setQuery(q_remote_movie)
            self.remote_sparql.setReturnFormat(JSON)
            remote_res = self.remote_sparql.query().convert()['results']['bindings']
            
            if("movies" in result):
                movies = result["movies"]
            else:
                movies = {}

            for item in remote_res:
                key = item["title"]["value"] + " - " + item["director_name"]["value"]
                val = item["link"]["value"]
                if(key in movies):
                    movies[key][1] = val
                else:
                    movies[key] = ["", val]
            
            result["movies"] = movies

        return result


if __name__ == "__main__":
    a = Helper()
    res = a.get_person_detail("","http://dbpedia.org/resource/Jonathan_Hyde")
    for key, val in res.items():
        print(key, " | ", val)
