from SPARQLWrapper import SPARQLWrapper, JSON


sparql = SPARQLWrapper("http://dbpedia.org/sparql")

keyword = 'the'
q = """
PREFIX ex: <http://example.org/data/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?title ?director_name ?link
WHERE {
    ?link a dbo:Film .
    ?link rdfs:label ?title .
    ?link dbo:director ?director .
    ?director rdfs:label ?director_name .
    FILTER (CONTAINS (LCASE(?title), "the")) .
    FILTER (lang(?title) = 'en') .
    FILTER (lang(?director_name) = 'en') .
}
LIMIT 10
"""

sparql.setQuery(q)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
res = results['results']['bindings']

# print(res)
# print("=================================")

for item in res:
    print(item['title']['value'],item['director_name']['value'],item['link']['value'])